#!/bin/bash
echo "Regenerating FaceUnlock..."

ROOTDIR="$PWD"
CURRENT_DIR="$ROOTDIR/external/motorola/faceunlock/regenerate"

cd packages/apps/Settings
git am $CURRENT_DIR/0001-4-4-Port-face-unlock-feature.patch

cd $ROOTDIR/frameworks/base
git am $CURRENT_DIR/0001-3-4-Port-face-unlock-feature.patch
git am $CURRENT_DIR/0002-base-do-not-use-new-lockscreen-layout-for-bypass.patch

cd $ROOTDIR
echo "Done"
